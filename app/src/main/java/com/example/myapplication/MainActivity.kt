package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var editText: TextView
    private lateinit var editText2: TextView
    private lateinit var editText3: TextView
    private lateinit var editText4: TextView
    private lateinit var editText5: TextView
    private lateinit var button: Button
    private lateinit var button2: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        editText = findViewById(R.id.editText)
        editText2 = findViewById(R.id.editText2)
        editText3 = findViewById(R.id.editText3)
        editText4 = findViewById(R.id.editText4)
        editText5 = findViewById(R.id.editText5)
        button = findViewById(R.id.button)
        button2 = findViewById(R.id.button2)

        button.setOnClickListener {
            val pi = editText.text.toString()
            val pi2 = editText2.text.toString()
            val pi3 = editText3.text.toString()
            val pi4 = editText4.text.toString()
            val pi5 = editText5.text.toString()
            val intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("User Name", pi)
            startActivity(intent)
        }

        button2.setOnClickListener {
            val pi = editText.text.toString()
            val pi2 = editText2.text.toString()
            val pi3 = editText3.text.toString()
            val pi4 = editText4.text.toString()
            val pi5 = editText5.text.toString()
            val intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("User Name", pi)
            startActivity(intent)
        }

    }

}
