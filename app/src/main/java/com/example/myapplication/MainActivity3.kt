package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity3 : AppCompatActivity() {

    private lateinit var password : TextView
    val correctPassword = "beka"
    var name = "beka"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)
        password = findViewById(R.id.editText6)

        findViewById<TextView>(R.id.text1).text = intent?.extras?.getString("User Name", "Hi $name")

    }
    fun check(view: View) {
        if (password.text == correctPassword){
            Toast.makeText(this, "Right!", Toast.LENGTH_SHORT).show()
        }else {
            Toast.makeText(this, "Wrong!", Toast.LENGTH_SHORT).show()
        }
    }

}
